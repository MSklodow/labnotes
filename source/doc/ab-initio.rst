.. _doc_ab-initio:

*ab-initio* and *ab-relax* protocols
====================================

Preparing input files for Rosetta
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Idealising input PDB file

    The following command reads in a PDB file and writes that protein in an ideal form, i.e. with all
    bond lenghts and planar angles according to the Rosetta definition
    
    .. code-block:: console

        idealize_jd2.macosclangrelease -in:file:s 1tenA.pdb 
      
      

Runing  *ab-initio*
~~~~~~~~~~~~~~~~~~~

Input flags to run ab-initio protocol

    .. code-block:: console

        -abinitio::rsd_wt_helix 0.5
        -abinitio::rsd_wt_loop 0.5
        -abinitio::rg_reweight 0.5
        -abinitio::use_filters false
        -abinitio::relax false
        -silent_gz

        -nstruct 1000
        -increase_cycles 1

        -frag3 1pqxA-multiR.3w.200.3mers
        -frag9 1pqxA-multiR.3w.200.9mers

        -in::file::native  1pqxA.pdb
        -in::file::s       1pqxA.pdb

        -score::sspair_promoted ../1pqx.NOE
        -score::sspair_promotion_factor 1.5

        -out:file:silent 1pqxA-abinitio.out
        -out:sf 1pqxA-abinitio.fsc

Running *ab-relax*
~~~~~~~~~~~~~~~~~~~~~~~

Just change ``-abinitio::relax false`` to ``-abinitio::relax true`` in the flag-file above

Using backbone NOE in centroid modeling 
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Add the two lines to the flag-file

    .. code-block:: console

        -score::sspair_promoted ../1pqx.NOE
        -score::sspair_promotion_factor 1.5


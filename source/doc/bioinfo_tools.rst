.. _bioinfo_tools:

Bioinformatics tools
====================================

Running ``blast``
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

  -  **Running** ``formatdb`` **to create blast database from a FASTA file**
    ``makeblastdb -in uniref90.fasta -dbtype prot -title uniref90 -parse_seqids``
    This will transform a set of sequences in ``.fasta`` format into a **blast** database

  -  **Running** ``psiblast``
    Example script that runs the new ``psiblast`` command is given below:
    
      .. code-block:: bash

        TARGET='2azaA'
        DIR=$TARGET
        psiblast -num_iterations 5 \
          -num_alignments 100000 \
          -num_descriptions 100000 \
          -max_hsps 0 \
          -inclusion_ethresh 0.000001 \
          -evalue 0.000001 \
          -db /home/bioshell_server/TOOLS/blast+/data/nr \
          -query ./$DIR/$TARGET.fasta \
          -show_gis  -outfmt 0 \
          -num_threads 12 \
          -out ./$DIR/$TARGET.psi \
          -out_pssm ./$DIR/$TARGET.asn1 \
          -out_ascii_pssm ./$DIR/$TARGET.fasta.mat

    On the contrary to the original ``blastpgp`` command, now the names of command line parameters are quite self-explanatory. One of the advantages of the new version of blast is its parallel implementation allowing run a single job below a minute. Resulting sequence profiles are now stored in ASN.1 format, which is supported by BioShell.

  -  **Extracting sequences from the database**
    ``blastdbcmd -db uniref50 -dbtype prot -entry_batch hits.txt -outfmt %f -out hits.fasta``



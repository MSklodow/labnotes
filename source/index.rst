Welcome to Gront lab laboratory protocols
===========================================================

This repository provides notes, protocols and tutorials related to multiscale
modeling biomacromolecules, primarily proteins and their complexes, that are
used in daily research tasks by Gront lab (http://bioshell.pl). While documentation
related to BioShell package (bioinformatics and biosimulation tools) can be found
on separate site: http://bioshell.readthedocs.io/, this pages document mainly
Rosetta, blast and other tools commonly used in the field.
   
.. toctree::
   :maxdepth: 1
   :caption: Rosetta protocols
   :name: Rosetta

   doc/fragment_picking
   doc/ab-initio
   
Rosetta testing server: http://benchmark.graylab.jhu.edu/

.. toctree::
   :maxdepth: 1
   :caption: Other notes
   :name: other

   doc/git-notes
   doc/bioinfo_tools

Other links
-----------

 - RestructuredText format guide: http://docutils.sourceforge.net/docs/user/rst/quickref.html
 - Sphinx documentation: http://www.sphinx-doc.org/en/master/
